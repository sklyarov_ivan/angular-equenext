module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'app/bower_components/angular/angular.js',
      // 'app/bower_components/angular-route/angular-route.js',
      "app/bower_components/angular-ui-router/release/angular-ui-router.js",
      "app/bower_components/angular-permission/dist/angular-permission.js",
      'app/bower_components/angular-mocks/angular-mocks.js',
      'app/components/**/*.js',
      // 'app/page_*/**/*.js',
      // 'app/services/**/*_test.js'
      'app/**/*.js',
      'unit tests/*.js'


    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],
    reporters: ['progress', 'coverage'],
    preprocessors :{
      'app/**/*.js': ['coverage']
    },
    plugins : [
            'karma-coverage',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],
    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },
    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};

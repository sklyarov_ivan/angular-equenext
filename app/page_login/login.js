'use strict';

angular.module('myApp.login', ['ui.router'])

.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('login', {
          url: "/login",
          templateUrl: "/app/page_login/login.html",
          controller: 'LoginCtrl',
          data: {
            permissions: {
              only: ['anonymous']
            }
          }
        });
}])

.controller('LoginCtrl', ['$scope','authModel','$state','validateModel','_',
  function($scope,authModel,$state,validateModel,_) {
    $scope.loginAction = function() {
        if (isValid()) {
            var params = {
                "username":$scope.login,
                "password":$scope.password
            };
            var success = function(data){
                    if (console) console.log('data',data);
                    authModel.setUser(data);
                    $state.go('dashboard')
            };
            var fail = function(error){
                    if (console) console.log('error',error);
                    if (error.error) {
                        $scope.error = error.error.message;
                    }
            };
            authModel.login(params,success,fail);
        }
    };
    var isValid = function(){
      var err = [];
      err.push(validateModel.pass_tooshort($scope.password));
      err.push(validateModel.email_required($scope.login));
      err = _.filter(err,function(i){return typeof i != 'undefined'});
      $scope.error = err.slice()[0];
      return !!!$scope.error;
    };

}]);
'use strict';
angular.module('myApp', [
  'ui.router',
  'ngCookies',
  'myApp.header',
  'myApp.home',
  'myApp.login',
  'myApp.register',
  'myApp.services',
  'myApp.dashboard',
  'permission',
  'underscore',
  'myApp.version',
])
.constant('APP_SETTINGS', {
    getUrl: function(url, vars) {
        return this.host + this.urls[url];
    },
    host: 'https://dev.equenext.net/api/v0.5',
    urls: {
        login:      '/auth',
        register:   '/users',
        logout:     "/auth", 
    }
})
.run(function (Permission, authModel) {
      Permission.defineRole('anonymous', function () {
        if (!authModel.isLoggedIn()) {
          return true; 
        }
        return false;
      });
})
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");
}]);

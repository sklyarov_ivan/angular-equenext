'use strict';

angular.module('myApp.header', ['ui.router'])

.controller('headerCtrl',['$scope','$state','authModel',function($scope,$state,authModel){
    $scope.isLoggedIn = authModel.isLoggedIn;
    $scope.logoutAction = function(){
        return authModel.logout(function(){
            $state.go('home');
        });

    } ;
}]);
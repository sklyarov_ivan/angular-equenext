'use strict';

myServices.factory('validateModel',[
    function(){
    var errors = {
      mismatch:       "password and confirmation password mismatch",
      tooshort:       "password too short (at least 4 symbol)",
      email_required: "email is required",
      agreement:      "agreement acceptance required"
    };

    var validateModel = {
        pass_mismatch: function(register_pass,register_pass2){
          if (register_pass != register_pass2) {
            return errors.mismatch;
          }
          return;
        },
        pass_tooshort: function(register_pass){
          if (!register_pass || register_pass.length < 4) {
            return errors.tooshort;
          }
        return;

        },
        email_required: function(register_email){
          var re = /(?:[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
          if (!register_email || !re.test(register_email)) {
            return errors.email_required;
          }
          return;
        },
        agreement: function(chk_agreed){
          if (!chk_agreed) {
            return errors.agreement;
          }
          return;
        }
    };
    return validateModel;
}]);
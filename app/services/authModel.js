'use strict';

myServices.factory('authModel',['$http','$q','APP_SETTINGS','$cookieStore',
    function($http,$q,APP_SETTINGS,$cookieStore){
     var cookieName = 'current_user_data';
    var auth =  {
        login: function(data,success,error){
            return $http
                .post(APP_SETTINGS.getUrl('login'),data)
                .success(success)
                .error(error);
        },
        register: function(data,success,error){
            return $http
                .post(APP_SETTINGS.getUrl('register'),data)
                .success(success)
                .error(error);
        },
        logout: function(cb){
            var self = this;
            if (this.isLoggedIn()) {
                return $http
                    .delete(APP_SETTINGS.getUrl('logout'),
                    {
                        headers: {'Authorization': 'Bearer '+self.getUser().access_token}
                    })
                    .success(function(data){
                        if (console) console.log(data);
                        self.removeUser();
                        if (cb) cb();
                    })
                    .error(function(error){
                        if(console) console.log('error',error);
                        self.removeUser();
                        if (cb) cb();
                    });
            } else {
                if (cb) cb();
            }
        },
        currentUser: function(){
            var self = this;

            return $http
                .get(APP_SETTINGS.getUrl('login'))
                .success(function(data){
                    self.setUser(data);
                })
                .error(function(error){
                    if (console) console.log('error',error);
                });
        },
        setUser: function(user){
            return $cookieStore.put(cookieName, user);
        },
        getUser: function(){
            return $cookieStore.get(cookieName) || null;
        },
        removeUser: function(){
            $cookieStore.remove(cookieName);
        },
        isLoggedIn: function(){
            if (!$cookieStore.get(cookieName)) {
                return false;
            } else {
                return true;
            }
        }
    };
    return auth;
}]);
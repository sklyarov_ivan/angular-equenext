'use strict';

angular.module('myApp.register', ['ui.router'])

.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('register', {
        url: "/register",
        templateUrl: "/app/page_register/register.html",
        controller: 'RegisterCtrl',
        data: {
          permissions: {
            only: ['anonymous']
          }
        }
    });
}])

.controller('RegisterCtrl', ['$scope','authModel','$state','validateModel','_', function($scope,authModel,$state,validateModel,_) {
    $scope.registerAction = function() {
        if (isValid()) {
            var params = {
                "email":        $scope.register_email,
                "password":     $scope.register_pass,
                "name":         $scope.register_name,
                "organization": $scope.register_org

            }
            var success = function(data){
                    if (console) console.log('data',data);
                    var params = {
                        "username":$scope.register_email,
                        "password":$scope.register_pass,

                    };
                    var success = function(data){
                            if (console) console.log('data',data);
                            authModel.setUser(data);
                            $state.go('dashboard')
                    };
                    var fail = function(error){
                            if (console) console.log('error',error);
                            if (error.error) {
                                $scope.error = error.error.message;
                            }
                    };
                    authModel.login(params,success,fail);
            };
            var fail = function(error){
                    if (console) console.log('error',error);
                    if (error.error) {
                        $scope.error = error.error.message;
                    }
            };
            authModel.register(params,success,fail);
        }
    };
    var isValid = function(){
      var err = [];
      err.push(validateModel.pass_mismatch($scope.register_pass,$scope.register_pass2));
      err.push(validateModel.pass_tooshort($scope.register_pass));
      err.push(validateModel.email_required($scope.register_email));
      err.push(validateModel.agreement($scope.chk_agreed));
      err = _.filter(err,function(i){return typeof i != 'undefined'});
      $scope.error = err.slice()[0];
      return !!!$scope.error;
    };
}]);
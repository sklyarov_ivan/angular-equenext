'use strict';

angular.module('myApp.dashboard', ['ui.router'])

.config(['$stateProvider', function($stateProvider) {
  // $routeProvider.when('/dashboard', {
  //   templateUrl: '/app/dashboard/dashboard.html',
  //   controller: 'DashboardCtrl'
  // });
 $stateProvider
    .state('dashboard', {
      url: "/",
      templateUrl: "/app/page_dashboard/dashboard.html",
      controller: 'DashboardCtrl',
      data: {
        permissions: {
          except: ['anonymous']
        }
      }
    });
}])

.controller('DashboardCtrl', ['$scope','authModel','$location',function($scope,authModel,$location) {
    // if (!auth.isLoggedIn()) $location.path('/');
    $scope.user = authModel.getUser();
}]);
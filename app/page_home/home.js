'use strict';

angular.module('myApp.home', ['ui.router'])

.config(['$stateProvider', function($stateProvider) {
   $stateProvider
    .state('home', {
      url: "/",
      templateUrl: "/app/page_home/home.html",
      controller: 'HomeCtrl',
      data: {
        permissions: {
          only: ['anonymous'],
          redirectTo: 'dashboard'
        }
      }
    });
}])

.controller('HomeCtrl', [function() {
}]);

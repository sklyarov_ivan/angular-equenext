'use strict';
describe("myApp Module:", function() {
    var module,state;
    beforeEach(function() {
        module = angular.mock.module('myApp');

        inject(function ($injector) {
            state = $injector.get('$state');
        });
    });

    describe('index route', function() {
        it('should respond to home URL', function() {
            expect(state.href('home')).toEqual('#/');
        });

        it('should respond to login URL', function() {
            expect(state.href('login')).toEqual('#/login');
        });

        it('should respond to register URL', function() {
            expect(state.href('register')).toEqual('#/register');
        });
    });
});
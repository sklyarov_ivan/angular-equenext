'use strict';

describe('Service: authModel', function () {
        // module('mean.system');
        var authModel,$httpBackend;
        beforeEach(function(){
            angular.mock.module('myApp');

            inject(function($injector) {
                $httpBackend = $injector.get('$httpBackend');

                authModel = $injector.get('authModel');
            });
            $httpBackend
              .whenGET('/app/page_home/home.html')
              .respond ({
                status: 200
            }); 

        });
        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should have an login function', function () { 
            $httpBackend.flush();
            expect(angular.isFunction(authModel.login)).toBe(true);
        });

         it('should have an register function', function () { 
            $httpBackend.flush();
             expect(angular.isFunction(authModel.register)).toBe(true);
         });

         it('should have an logout function', function () {
            $httpBackend.flush();
             expect(angular.isFunction(authModel.logout)).toBe(true);
         });

         it('should have an currentUser function', function () {
            $httpBackend.flush();
             expect(angular.isFunction(authModel.currentUser)).toBe(true);
         });

         it('should have an setUser function', function () {
            $httpBackend.flush();
             expect(angular.isFunction(authModel.setUser)).toBe(true);
         });

         it('should have an removeUser function', function () {
            $httpBackend.flush();
             expect(angular.isFunction(authModel.removeUser)).toBe(true);
         });

         it('should have an isLoggedIn function', function () {
            $httpBackend.flush();
             expect(angular.isFunction(authModel.isLoggedIn)).toBe(true);
         });

         it('should return isLoggedIn false', function () {
             authModel.removeUser();
             $httpBackend.flush();
             expect(authModel.isLoggedIn()).toBe(false);
         });

          it('should return isLoggedIn true', function () {
            $httpBackend
              .whenGET('/app/page_dashboard/dashboard.html')
              .respond ({
                status: 200
            }); 
             authModel.setUser({
                access_token: 'YlJmjvC2mxtsp5UXuPDu',
                token_type: "bearer",
                expires_in: 86400,
                etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
             });
             $httpBackend.flush();
             expect(authModel.isLoggedIn()).toBe(true);
         });

        it('should return user', function () {
            $httpBackend
              .whenGET('/app/page_dashboard/dashboard.html')
              .respond ({
                status: 200
            }); 
              var access_token = 'YlJmjvC2mxtsp5UXuPDu';
             authModel.setUser({
                access_token: access_token,
                token_type: "bearer",
                expires_in: 86400,
                etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
             });
             $httpBackend.flush();
             expect(authModel.getUser().access_token).toEqual(access_token);
         });

        it('should test currentUser', function () {
                $httpBackend
                  .whenGET('/app/page_dashboard/dashboard.html')
                  .respond ({
                    status: 200
                });
                var access_token = 'YlJmjvC2mxtsp5UXuPDu';
                $httpBackend
                .expectGET('https://dev.equenext.net/api/v0.5/auth')
                .respond (200,{
                  access_token: access_token,
                  token_type: "bearer",
                  expires_in: 86400,
                  etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
                }); 
             authModel.currentUser();

             $httpBackend.flush();
             expect(authModel.getUser().access_token).toEqual(access_token);
         });

        it('should test logout', function () {
                $httpBackend
                  .whenGET('/app/page_dashboard/dashboard.html')
                  .respond ({
                    status: 200
                });
                authModel.setUser({
                    access_token: 'YlJmjvC2mxtsp5UXuPDu',
                    token_type: "bearer",
                    expires_in: 86400,
                    etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
                 });
                $httpBackend
                  .expectDELETE('https://dev.equenext.net/api/v0.5/auth')
                  .respond (200,{
                    ok: "user logged out",
                    etag: "1a24bbfa5251ef3f9791305075a2fded"
                }); 
             authModel.logout();

             $httpBackend.flush();
             expect(authModel.isLoggedIn()).toBe(false);
        });

        it('should test logout', function () {
                $httpBackend
                  .whenGET('/app/page_dashboard/dashboard.html')
                  .respond ({
                    status: 200
                });
                authModel.setUser({
                    access_token: 'YlJmjvC2mxtsp5UXuPDu',
                    token_type: "bearer",
                    expires_in: 86400,
                    etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
                 });
                $httpBackend
                  .expectDELETE('https://dev.equenext.net/api/v0.5/auth')
                  .respond (422,{
                    etag: "6871a4d04c3f70a858e651417617ad67",
                    error: {
                        code: 422,
                        message : 'You need to submit a correct email address'
                    }
                }); 
             authModel.logout();

             $httpBackend.flush();
             expect(authModel.isLoggedIn()).toBe(false);
        });

});
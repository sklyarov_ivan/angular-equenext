'use strict';
describe("header controller:", function() {
    var $controller,headerCtrl,$scope,$rootScope,$state,authModel,createController,createControllerWithUser,$httpBackend;
    beforeEach(function() {
        angular.mock.module('myApp');

        inject(function ($injector) {
            $state = $injector.get('$state');
            $controller = $injector.get('$controller');
            $rootScope = $injector.get('$rootScope');
            authModel = $injector.get('authModel');
            $scope = $rootScope.$new();
            $httpBackend = $injector.get('$httpBackend');
            createController = function () {
                headerCtrl = $controller('headerCtrl',{
                    $scope:         $scope,
                    authModel:      authModel,
                    $state:         $state
                  });
                return headerCtrl;
            };
            $httpBackend
              .whenGET('/app/page_dashboard/dashboard.html')
              .respond ({
                status: 200
              }); 
            $httpBackend
              .whenGET('/app/page_home/home.html')
              .respond ({
                status: 200
              }); 
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe('logoutAction', function() {
        it('should be function', function() {
            createController();
            $httpBackend.flush();
            expect(typeof $scope.logoutAction).toEqual('function');
        });

        it('should go to home page', function() {
            createController();
            $scope.logoutAction();
            $httpBackend.flush();
            authModel.removeUser();
            expect(authModel.isLoggedIn()).toEqual(false);
            expect($state.href('home')).toEqual('#/');
        });

        describe('set user',function(){
            it('should make logout with success response', function() {
                authModel.setUser({
                    access_token: 'YlJmjvC2mxtsp5UXuPDu',
                    token_type: "bearer",
                    expires_in: 86400,
                    etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
                });
                expect(authModel.isLoggedIn()).toEqual(true);
                createController();
                $httpBackend
                  .expectDELETE('https://dev.equenext.net/api/v0.5/auth')
                  .respond (200,{
                    ok: "user logged out",
                    etag: "1a24bbfa5251ef3f9791305075a2fded"
                }); 

                $scope.logoutAction();
                $httpBackend.flush();
                expect(authModel.isLoggedIn()).toEqual(false);
                expect($state.href('home')).toEqual('#/');
            });

            it('should make logout with success response', function() {
                authModel.setUser({
                    access_token: 'YlJmjvC2mxtsp5UXuPDu',
                    token_type: "bearer",
                    expires_in: 86400,
                    etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
                });
                expect(authModel.isLoggedIn()).toEqual(true);
                createController();
                $httpBackend
                  .expectDELETE('https://dev.equenext.net/api/v0.5/auth')
                  .respond (422,{
                    etag: "6871a4d04c3f70a858e651417617ad67",
                    error: {
                        code: 422,
                        message : 'You need to submit a correct email address'
                    }
                }); 

                $scope.logoutAction();
                $httpBackend.flush();
                expect(authModel.isLoggedIn()).toEqual(false);
                expect($state.href('home')).toEqual('#/');
            });
        });

    });

});
'use strict';
describe("login controller:", function() {
    var module,$controller,LoginCtrl,$scope,$rootScope,$state,authModel,createController,$httpBackend,validateModel,_;
    beforeEach(function() {
        module = angular.mock.module('myApp');

        inject(function ($injector) {
            $state = $injector.get('$state');
            $controller = $injector.get('$controller');
            $rootScope = $injector.get('$rootScope');
            authModel = $injector.get('authModel');
            $scope = $rootScope.$new();
            $httpBackend = $injector.get('$httpBackend');
            validateModel = $injector.get('validateModel');
            _ = $injector.get('_');
            createController = function () {
                LoginCtrl = $controller('LoginCtrl',{
                    $scope:         $scope,
                    authModel:      authModel,
                    $state:         $state,
                    validateModel:  validateModel,
                    _:              _
                  });
                return LoginCtrl;
            };
            $httpBackend
              .whenGET('/app/page_home/home.html')
              .respond ({
                status: 200
              }); 
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe('loginAction', function() {
        it('should be function', function() {
            createController();
            $httpBackend.flush();
            expect(typeof $scope.loginAction).toEqual('function');
        });

        it('should check password length',function(){
          $scope.password = '123';
          createController();
          $scope.loginAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/password too short/);
        });

        it('should check email required',function(){
          $scope.password = '1234';
          createController();
          $scope.loginAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/email is required/);
        });


        it('should run function with fail',function(){
            $scope.password = '1234';
            $scope.login = 'test@example.com';
            $httpBackend
              .expectPOST('https://dev.equenext.net/api/v0.5/auth')
              .respond (422,{
                etag: "6871a4d04c3f70a858e651417617ad67",
                error: {
                    code: 422,
                    message : 'You need to submit a correct email address'
                }
              });
            createController();
            $scope.loginAction();
            $httpBackend.flush();
            expect($scope.error).toMatch(/You need to submit a correct email address/);
        });

        it('should run function success',function(){
            $scope.password = '1234';
            $scope.login = 'test@example.com';
            $httpBackend
              .expectPOST('https://dev.equenext.net/api/v0.5/auth')
              .respond (200,{
                access_token: 'YlJmjvC2mxtsp5UXuPDu',
                token_type: "bearer",
                expires_in: 86400,
                etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
              }); 
            $httpBackend
              .expectGET('/app/page_dashboard/dashboard.html')
              .respond (200,{
              });
            createController();
            $scope.loginAction();
            $httpBackend.flush();
            expect($state.href('dashboard')).toEqual('#/');
        });

        it('should check stored data',function(){
            $scope.password = '1234';
            $scope.login = 'test@example.com';
            var access_token = 'YlJmjvC2mxtsp5UXuPDu';
            $httpBackend
              .expectPOST('https://dev.equenext.net/api/v0.5/auth')
              .respond (200,{
                access_token: access_token,
                token_type: "bearer",
                expires_in: 86400,
                etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
              }); 
            $httpBackend
              .expectGET('/app/page_dashboard/dashboard.html')
              .respond (200,{
              }); 
            createController();
            $scope.loginAction();
            $httpBackend.flush();
            expect(authModel.getUser().access_token).toEqual(access_token);
        });
    });
});
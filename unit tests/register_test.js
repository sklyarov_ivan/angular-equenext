'use strict';
describe("register controller:", function() {
    var module,$controller,RegisterCtrl,$scope,$rootScope,$state,authModel,createController,$httpBackend,validateModel,_;
    beforeEach(function() {
        module = angular.mock.module('myApp');

        inject(function ($injector) {
            $state = $injector.get('$state');
            $controller = $injector.get('$controller');
            $rootScope = $injector.get('$rootScope');
            authModel = $injector.get('authModel');
            validateModel = $injector.get('validateModel');
            _ = $injector.get('_');

            $scope = $rootScope.$new();
            $httpBackend = $injector.get('$httpBackend');
            createController = function () {
                RegisterCtrl = $controller('RegisterCtrl',{
                    $scope:         $scope,
                    authModel:      authModel,
                    $state:         $state,
                    validateModel:  validateModel,
                    _:              _
                  });
                return RegisterCtrl;
            };
            $httpBackend
              .whenGET('/app/page_home/home.html')
              .respond ({
                status: 200
              }); 
        });
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });
    describe('RegisterCtrl', function() {
        it('should be function', function() {
            createController();
            $httpBackend.flush();
            expect(typeof $scope.registerAction).toEqual('function');
        });

        it('should check password and confirmation password mismatch',function(){
          $scope.register_pass = '1234';
          $scope.register_pass2 = '4321';
          createController();
          $scope.registerAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/password and confirmation password mismatch/);
        });

        it('should check password and confirmation password mismatch',function(){
          $scope.register_pass = '123';
          $scope.register_pass2 = '123';
          $scope.register_email = 'test@example.com';
          $scope.chk_agreed = true;
          createController();
          $scope.registerAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/password too short/);
        });

        it('should check email is required',function(){
          $scope.register_pass = '1234';
          $scope.register_pass2 = '1234';
          createController();
          $scope.registerAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/email is required/);
        });

        it('should check email is required valid',function(){
          $scope.register_pass = '1234';
          $scope.register_pass2 = '1234';
          $scope.register_email = 'wrong@email';
          createController();
          $scope.registerAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/email is required/);
        });

        it('should check email is required 2',function(){
          $scope.register_pass = '1234';
          $scope.register_pass2 = '1234';
          $scope.register_email = 'test@example.com';
          createController();
          $scope.registerAction();
          $httpBackend.flush();
          expect($scope.error).toMatch(/agreement acceptance required/);
        });

        it('should run function with fail',function(){
          $scope.register_pass = '1234';
          $scope.register_pass2 = '1234';
          $scope.register_email = 'test@example.com';
          $scope.chk_agreed = true;
            $httpBackend
              .expectPOST('https://dev.equenext.net/api/v0.5/users')
              .respond (422,{
                etag: "6871a4d04c3f70a858e651417617ad67",
                error: {
                    code: 422,
                    message : 'You need to submit a correct email address'
                }
              }); 
            createController();
            $scope.login = '';
            $scope.password = '';
            $scope.registerAction();
            $httpBackend.flush();
            expect($scope.error).toMatch(/You need to submit a correct email address/);
        });

        it('should run function success',function(){
          $scope.register_pass = '1234';
          $scope.register_pass2 = '1234';
          $scope.register_email = 'test@example.com';
          $scope.chk_agreed = true;
          var access_token = 'YlJmjvC2mxtsp5UXuPDu';
            $httpBackend
              .expectPOST('https://dev.equenext.net/api/v0.5/users')
              .respond (200,{
                id: "5448dd614733976e025a40c2",
                ok: "user created successfully",
                etag: "e2c6cab6d0a0dc436480d80a2256af07"
              });
            $httpBackend
              .expectPOST('https://dev.equenext.net/api/v0.5/auth')
              .respond (200,{
                access_token: access_token,
                token_type: "bearer",
                expires_in: 86400,
                etag: "fe5a9e73aff3b9e0e3cfe617b1013ca5"
              }); 
            $httpBackend
              .expectGET('/app/page_dashboard/dashboard.html')
              .respond (200,{
              }); 
            createController();
            $scope.login = 'login';
            $scope.password = 'pass';
            $scope.registerAction();
            $httpBackend.flush();
            expect($state.href('dashboard')).toEqual('#/');
            expect(authModel.getUser().access_token).toEqual(access_token);
        });
    });
});